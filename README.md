# tf2-killsounds

## Installing

Copy the killsound folder of your choice into `Team Fortress 2\tf\custom\`.

To properly apply the killsound, run `exec custom-killsound` from the console or place that command somewhere in your `autoexec.cfg`.

If done correctly, you should be able to see
```
==============================
= Loaded <NAME OF KILLSOUND> =
==============================
```
in the console.
